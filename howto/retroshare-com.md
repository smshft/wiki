# Documentació Retroshare: programari per compartir xifradament

Permet xat, compartir fitxers, ... tot de forma xifrada, amb varis nivells d'anonimicitat.
No destaca per la seva velocitat, però sí per ser un software distribuit.
Utilitza claus gpg que gestiona de forma molt simple per l'usuari, encara que permet ordres directes de consola.

Hem separat la documentació en tres parts:

## connecta't, confia, comparteix

- [connecta't] (./retroshare-connecta.md) 
   - Aqui recollim tot el que cal fer per instal.lar el software, connectar-se, tipus de connexions i manteniment general 
- [confia] (./retroshare-confia.md) 
   - Sobre les relacions de confiança, com gestionarles... 
- [comparteix] (./retroshare-comparteix.md) 
   - Bones pràctiques per compartir.. 
