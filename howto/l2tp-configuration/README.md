<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Configuració de túnels L2TP per routers OpenWRT/LEDE](#configuraci%C3%B3-de-t%C3%BAnels-l2tp-per-routers-openwrtlede)
  - [Instal·lació dels paquets necessaris](#instal%C2%B7laci%C3%B3-dels-paquets-necessaris)
  - [Configuració de la connexió a Guifi.net](#configuraci%C3%B3-de-la-connexi%C3%B3-a-guifinet)
  - [Configuració de la connexió L2TP d'accés a Internet IPv4](#configuraci%C3%B3-de-la-connexi%C3%B3-l2tp-dacc%C3%A9s-a-internet-ipv4)
  - [Configuració de la connexió d'accés a Internet per IPv6](#configuraci%C3%B3-de-la-connexi%C3%B3-dacc%C3%A9s-a-internet-per-ipv6)
  - [Activació de reconnexió automàtica](#activaci%C3%B3-de-reconnexi%C3%B3-autom%C3%A0tica)
- [work in progress - Configuració de túnels L2TP per debian i derivats (inclou edgemax)](#work-in-progress---configuraci%C3%B3-de-t%C3%BAnels-l2tp-per-debian-i-derivats-inclou-edgemax)
  - [TODO report bug to debian? result_code_avp: avp is incorrect size](#todo-report-bug-to-debian-result_code_avp-avp-is-incorrect-size)
  - [bug: out of order when trying to route traffic through the tunnel](#bug-out-of-order-when-trying-to-route-traffic-through-the-tunnel)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Configuració de túnels L2TP per routers OpenWRT/LEDE

Autor: Víctor Oncins 2017-11-12

El servei d'accés a Internet a través de l'eXO amb el protocol L2TP presenta certs avantatges. D'una banda millora els processos de gestió i control de la subscripció a Internet. De l'altra, simplifica la configuració dels routers residencials i permet integrar el suport de IPv4 i IPv6 a través d'una sola connexió. Els routers que suporten el sistema OpenWRT/LEDE permeten aquest tipus de connexió. Tanmateix hi ha routers amb firmwares propietaris que també el suporten. Si teniu un router residencial amb el firmware original, comproveu que el protocol L2TP està suportat.

El protocol L2TP encapsula una connexió de capa d'enllaç de tipus PPP. Per obtenir més informació cliqueu el document de referència [LAA](https://github.com/guifi-exo/wiki/blob/master/howto/arquitectura-laa-acces-inet.md). La connexió PPP requereix un nom d'usuari i un password que serà proveït per l'associació eXO. Un cop disposem d'aquestes dades ja podem configurar el router OpenWRT. Els esquemes d'autenticació suportats pel concentrador són PAP, CHAP i MSCHAP.

Aquesta és una guia tècnica per configurar aquest servei en routers residencials amb OpenWRT/LEDE. **Aquest document cobreix les versions estables d'OpenWRT 15.05.1 (Chaos Calmer) i LEDE 17.01.4 (Reboot).**

Podem trobar les imatges pre-compilades al dipòsit del projecte [OpenWRT](https://downloads.openwrt.org/chaos_calmer/15.05.1/) o [LEDE](https://downloads.lede-project.org/releases/17.01.4/). Trieu l'arquitectura de hardware i el model de router i procediu a gravar el nou firmware seguint les recomanacions dels webs dels respectius projectes. **Cal tenir en compte que no es recomana fer ús de dispositius de 4M de flash i 32M de RAM o menys.**

## Instal·lació dels paquets necessaris

Suposem que tenim un router amb un firmware OpenWRT amb les configuracions per defecte. Un cop el tinguem a punt, connecteu el port WAN a una connexió a Internet. Si els valors de la configuració són els per defecte, hi haurà un client DHCP activat. Entrem al web de configuració http://192.168.1.1/. Entrem a la part *System > Software*. Cliquem *Update lists* i actualitzem la llista de dipòsits.

Cerquem el paquet *xl2tpd* i l'instal·lem. De manera opcional podem instal·lar d'altres com ara el *ip* o el *tcpdump* que poden ser útils en una fase posterior de depuració de problemes. Si feu servir LEDE el paquet *ip* ja ve instal·lat. Alternativament podem instal·lar tots els paquets de cop per línia de comandes:

```
root@OpenWrt:~# opkg update
root@OpenWrt:~# opkg install xl2tpd ip tcpdump-mini
```

## Configuració de la connexió a Guifi.net

Cal tenir clar quina és la IP i rang (màscara) del node comunitari, típicament situat al terrat. Consulteu la pàgina web de Guifi.net o bé accediu directament al node. Normalment la IP té el format `10.a.b.c/27`. Reservem una nova IP dins d'aquest rang per router residencial, per exemple la `10.a.b.c+1/27`.

Anem a la part *Interfaces* i editem la WAN. Triem en el desplegable *Static address* i cliquem *Switch protocol*. Emplenem els camps amb aquests valors:

```
IPv4 address = 10.a.b.c+1
IPv4 netmask = 255.255.255.224
IPv4 gateway = 10.a.b.c
```

A l'apartat *Firewall Settings*, vinculem la interfície de xarxa a la zona WAN del firewall. Això impedirà l'accés de les connexions entrants al router residencial. Apliquem i desem canvis. Connectem el port que correspongui a la interfície WAN al cable del node comunitari o a l'equipment de xarxa que hi permeti l'accés. Assegureu-vos que teniu accés al concentrador de túnels de l'eXO:

```
root@OpenWrt:~# ping 10.38.140.225
PING 10.38.140.225 (10.38.140.225): 56 data bytes
64 bytes from 10.38.140.225: seq=0 ttl=61 time=6.780 ms
64 bytes from 10.38.140.225: seq=1 ttl=61 time=4.980 ms
^C
--- 10.38.140.225 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max = 4.980/5.880/6.780 ms
```

## Configuració de la connexió L2TP d'accés a Internet IPv4

Ens situem a la part de *Interfaces* i afegim una nova interfície clicant el botó *Add interface*. L'anomenem `exo`. Triem com a protocol de la nova interfície el L2TP. No hi vinculem cap interfície física. Apliquem els canvis i apareixeran els camps que caldrà emplenar:

```
L2TP Server = 10.38.140.225
PAP/CHAP username = <nom d'usuari proveït per eXO>
PAP/CHAP password = <password proveït per eXO>
```
A la secció *Advanced Settings*, marquem els punts següents:

- [X] Bring up on boot
- [x] Use builtin IPv6-management
- [ ] Trieu l'opció *Manual* del desplegable si feu servir LEDE
- [x] Use default gateway
- [x] Use DNS servers advertised by peer (Opcional)

**Si som en una xarxa de tipus mesh, cal que `Override MTU = 1420`, en cas contrari `Override MTU = 1436`.** Situem la nova interfície a la zona WAN del firewall a la part *Firewall Settings*. Apliquem i desem canvis.

## Configuració de la connexió d'accés a Internet per IPv6

L'eXO també proveeix d'accés a Internet per IPv6. En primer lloc caldrà suprimer el prefixe ULA que ve per defecte. Deixem en blanc el camp *IPv6 ULA-Prefix* a la part inferior de *Interfaces*. Per activar IPv6 global caldrà editar la interfície WAN6. Anem a *Physical Settings* i activem la següent opció tot emplenant el camp de text amb el valor `@exo`

- [X] Custom interface [ @exo ]

Apliqueu i deseu canvis. Per tal de fer efectiva la delegació del prefix IPv6 provinent del concentrador, caldrà editar la interfície BR-LAN i modificar el camp següent:

```
IPv6 assigment length = 64
```
Deseu, apliqueu canvis i reinicieu el router. Un cop recuperat, ja hauríeu de poder accedir a Internet per IPv4 i IPv6.

## Activació de reconnexió automàtica

Per defecte la reconnexió del túnel en cas d'interrupció de la comunicació està deshabilitada. Per activar-la caldrà editar l'arxiu `/etc/config/network` i afegir el camp `chekup_interval` amb el valor en segons del temps de reintent de connexió. Podem afegir també el camp `keepalive` per modular les comprovacions periòdiques de l'estat del túnel. En el nostre exemple, cada 10 segons enviarà un paquet de keepalive. Si durant 20 segons no es rep cap altre keepalive el túnel es considera desconnectat. Tornarà a fer intents cada 10 segons per tornar-lo a aixecar.

```
config interface 'exo'
        option proto 'l2tp'
        option server '10.38.140.225'
        option username '<el-vostre-usuari>'
        option password '<el-vostre-password>'
        option ipv6 '1'
        option mtu <el-vostre-mtu>
        option checkup_interval '10'
        option keepalive '20,10'
```

Un cop desats els canvis, reinicieu el router i ja tindrem la funció de reestabliment habilitada i la resta de configuracions aplicades.

# work in progress - Configuració de túnels L2TP per debian i derivats (inclou edgemax)

Nota1: per utilitzar edgemax com un debian executar a l'inici `sudo su` i per manegar el servei `/etc/init.d/xl2tpd` enlloc de `service xl2tpd`

Guardar un backup de la configuració exemple

    mv /etc/xl2tpd/xl2tpd.conf /etc/xl2tpd/xl2tpd.conf.bak

Editar el fitxer i copiem el següent contingut

    vi /etc/xl2tpd/xl2tpd.conf

Amb el contingut:

```
[global]
port = 1701
access control = no
auth file = /etc/ppp/chap-secrets
debug avp = no ; enable for debug

[lac exo]
lns = 10.38.140.225
redial = yes
redial timeout = 5
require chap = yes
ppp debug = no ; enable for debug
require pap = no
autodial = yes
name = <el-vostre-usuari>
refuse pap = yes
```

Afegir el secret (password) en el lloc corresponent

    vi /etc/ppp/chap-secrets

Amb contingut

```
"<el-vostre-usuari>" * "<el-vostre-password>"
```

Modificar el fitxer `/etc/ppp/options` segons el diff de la subsecció/bug detectat de debian, o [descarregar el fitxer aquí](./options)

## TODO report bug to debian? result_code_avp: avp is incorrect size

Problem: Default configuration of `/etc/ppp/options` gives this error connecting to mikrotik l2tp server. But applying default configuration with edgemax looks fine

TODO: understand the relevant part that breaks it

```diff
@@ -32,11 +32,12 @@
 # Please do not disable this setting. It is expected to be standard in
 # future releases of pppd. Use the call option (see manpage) to disable
 # authentication for specific peers.
-auth
+#auth
 
 # Use hardware flow control (i.e. RTS/CTS) to control the flow of data
 # on the serial port.
-crtscts
+#crtscts
+nocrtscts
 
 # Use software flow control (i.e. XON/XOFF) to control the flow of data
 # on the serial port.
@@ -70,7 +71,7 @@
 # Use the modem control lines.  On Ultrix, this option implies hardware
 # flow control, as for the crtscts option.  (This option is not fully
 # implemented.)
-modem
+#modem
 
 # Set the MRU [Maximum Receive Unit] value to <n> for negotiation.  pppd
 # will ask the peer to send packets of no more than <n> bytes. The
@@ -94,7 +95,7 @@
 # attempt to initiate a connection; if no reply is received from the
 # peer, pppd will then just wait passively for a valid LCP packet from
 # the peer (instead of exiting, as it does without this option).
-#passive
+passive
 
 # With this option, pppd will not transmit LCP packets to initiate a
 # connection until a valid LCP packet is received from the peer (as for
@@ -115,7 +116,7 @@
 
 # Don't fork to become a background process (otherwise pppd will do so
 # if a serial device is specified).
-#-detach
+-detach
 
 # Disable IP address negotiation (with this option, the remote IP
 # address must be specified with an option on the command line or in
@@ -176,7 +177,7 @@
 # general debug messages, 2 to request that the contents of received
 # packets be printed, and 4 to request that the contents of transmitted
 # packets be printed.
-#kdebug n
+#kdebug 7
 
 # Set the MTU [Maximum Transmit Unit] value to <n>. Unless the peer
 # requests a smaller value via MRU negotiation, pppd will request that
@@ -204,7 +205,7 @@
 # Add an entry to this system's ARP [Address Resolution Protocol]
 # table with the IP address of the peer and the Ethernet address of this
 # system.
-#proxyarp
+proxyarp
 
 # Use the system password database for authenticating the peer using
 # PAP. Note: mgetty already provides this option. If this is specified
@@ -215,7 +216,7 @@
 # peer every n seconds. Normally the peer should respond to the echo-request
 # by sending an echo-reply. This option can be used with the
 # lcp-echo-failure option to detect that the peer is no longer connected.
-lcp-echo-interval 30
+#lcp-echo-interval 30
 
 # If this option is given, pppd will presume the peer to be dead if n
 # LCP echo-requests are sent without receiving a valid LCP echo-reply.
@@ -224,7 +225,7 @@
 # This option can be used to enable pppd to terminate after the physical
 # connection has been broken (e.g., the modem has hung up) in
 # situations where no hardware modem control lines are available.
-lcp-echo-failure 4
+#lcp-echo-failure 4
 
 # Set the LCP restart interval (retransmission timeout) to <n> seconds
 # (default 3).
@@ -298,7 +299,7 @@
 
 # Exit once a connection has been made and terminated. This is the default,
 # unless the `persist' or `demand' option has been specified.
-#nopersist
+nopersist
 
 # Do not exit after a connection is terminated; instead try to reopen
 # the connection.
@@ -306,7 +307,7 @@
 
 # Terminate after n consecutive failed connection attempts.
 # A value of 0 means no limit. The default value is 10.
-#maxfail <n>
+maxfail 0
 
 # Initiate the link only on demand, i.e. when data traffic is present. 
 # With this option, the remote IP address must be specified by the user on
@@ -345,4 +346,12 @@
 # The filter expression is akin to that of tcpdump(1)
 #active-filter <filter-expression>
 
+# Vyatta selected options
+ipcp-accept-local
+ipcp-accept-remote
+mppe-stateful
+updetach
+ktune
+lcp-max-configure 99999
+
 # ---<End of File>---
```

error as seen in `/var/log/daemon.log` with the incorrect `/etc/ppp/options` problem

```
Aug  9 18:41:17 localhost systemd[1]: Stopped LSB: layer 2 tunelling protocol daemon.
Aug  9 18:41:17 localhost systemd[1]: Starting LSB: layer 2 tunelling protocol daemon...
Aug  9 18:41:17 localhost xl2tpd[31113]: Not looking for kernel SAref support.
Aug  9 18:41:17 localhost xl2tpd[31113]: Using l2tp kernel support.
Aug  9 18:41:17 localhost xl2tpd[31110]: Starting xl2tpd: xl2tpd.
Aug  9 18:41:17 localhost xl2tpd[31114]: xl2tpd version xl2tpd-1.3.12 started on host PID:31114
Aug  9 18:41:17 localhost systemd[1]: Started LSB: layer 2 tunelling protocol daemon.
Aug  9 18:41:17 localhost xl2tpd[31114]: Written by Mark Spencer, Copyright (C) 1998, Adtran, Inc.
Aug  9 18:41:17 localhost xl2tpd[31114]: Forked by Scott Balmos and David Stipp, (C) 2001
Aug  9 18:41:17 localhost xl2tpd[31114]: Inherited by Jeff McAdams, (C) 2002
Aug  9 18:41:17 localhost xl2tpd[31114]: Forked again by Xelerance (www.xelerance.com) (C) 2006-2016
Aug  9 18:41:17 localhost xl2tpd[31114]: Listening on IP address 0.0.0.0, port 1701
Aug  9 18:41:17 localhost xl2tpd[31114]: Connecting to host [masked_ip_l2tp_server], port 1701
Aug  9 18:41:17 localhost xl2tpd[31114]: message_type_avp: message type 2 (Start-Control-Connection-Reply)
Aug  9 18:41:17 localhost xl2tpd[31114]: protocol_version_avp: peer is using version 1, revision 0.
Aug  9 18:41:17 localhost xl2tpd[31114]: framing_caps_avp: supported peer frames: sync
Aug  9 18:41:17 localhost xl2tpd[31114]: bearer_caps_avp: supported peer bearers:
Aug  9 18:41:17 localhost xl2tpd[31114]: firmware_rev_avp: peer reports firmware version 1 (0x0001)
Aug  9 18:41:17 localhost xl2tpd[31114]: hostname_avp: peer reports hostname '[masked_hostname_l2tp_server]'
Aug  9 18:41:17 localhost xl2tpd[31114]: vendor_avp: peer reports vendor 'MikroTik'
Aug  9 18:41:17 localhost xl2tpd[31114]: assigned_tunnel_avp: using peer's tunnel 2988
Aug  9 18:41:17 localhost xl2tpd[31114]: receive_window_size_avp: peer wants RWS of 4.  Will use flow control.
Aug  9 18:41:17 localhost xl2tpd[31114]: Connection established to [masked_ip_l2tp_server], 1701.  Local: 27991, Remote: 2988 (ref=0/0).
Aug  9 18:41:17 localhost xl2tpd[31114]: Calling on tunnel 27991
Aug  9 18:41:17 localhost xl2tpd[31114]: message_type_avp: message type 11 (Incoming-Call-Reply)
Aug  9 18:41:17 localhost xl2tpd[31114]: assigned_call_avp: using peer's call 1
Aug  9 18:41:17 localhost xl2tpd[31114]: Call established with [masked_ip_l2tp_server], Local: 31084, Remote: 1, Serial: 1 (ref=0/0)
Aug  9 18:41:17 localhost xl2tpd[31114]: start_pppd: I'm running:
Aug  9 18:41:17 localhost xl2tpd[31114]: "/usr/sbin/pppd"
Aug  9 18:41:17 localhost xl2tpd[31114]: "plugin"
Aug  9 18:41:17 localhost xl2tpd[31114]: "pppol2tp.so"
Aug  9 18:41:17 localhost xl2tpd[31114]: "pppol2tp"
Aug  9 18:41:17 localhost xl2tpd[31114]: "7"
Aug  9 18:41:17 localhost xl2tpd[31114]: "passive"
Aug  9 18:41:17 localhost xl2tpd[31114]: "nodetach"
Aug  9 18:41:17 localhost xl2tpd[31114]: ":"
Aug  9 18:41:17 localhost xl2tpd[31114]: "refuse-pap"
Aug  9 18:41:17 localhost xl2tpd[31114]: "name"
Aug  9 18:41:17 localhost xl2tpd[31114]: "[masked_name_l2tp_client]"
Aug  9 18:41:17 localhost xl2tpd[31114]: "debug"
Aug  9 18:41:17 localhost xl2tpd[31114]: child_handler : pppd exited for call 1 with code 1
Aug  9 18:41:17 localhost xl2tpd[31114]: call_close: Call 31084 to [masked_ip_l2tp_server] disconnected
Aug  9 18:41:17 localhost xl2tpd[31114]: message_type_avp: message type 4 (Stop-Control-Connection-Notification)
Aug  9 18:41:17 localhost xl2tpd[31114]: result_code_avp: avp is incorrect size.  8 < 10
Aug  9 18:41:17 localhost xl2tpd[31114]: handle_avps: Bad exit status handling attribute 1 (Result Code) on mandatory packet.
Aug  9 18:41:17 localhost xl2tpd[31114]: Terminating pppd: sending TERM signal to pid 31115
Aug  9 18:41:17 localhost xl2tpd[31114]: Connection 2988 closed to [masked_ip_l2tp_server], port 1701 (Result Code: expected at least 10, got 8)
```

src https://github.com/xelerance/xl2tpd/issues/90

src https://github.com/xelerance/xl2tpd/issues/130

## bug: out of order when trying to route traffic through the tunnel

after the connection l2tp is established it cannot route traffic through it

```
sudo ip r a default dev ppp0
sudo iptables --policy FORWARD ACCEPT
```

```
Aug  9 19:44:17 localhost xl2tpd[7385]: message_type_avp: message type 6 (Hello)
Aug  9 19:44:18 localhost xl2tpd[7385]: check_control: Received out of order control packet on tunnel 3471 (got 2, expected 3)
Aug  9 19:44:18 localhost xl2tpd[7385]: handle_packet: bad control packet!
Aug  9 19:44:19 localhost xl2tpd[7385]: check_control: Received out of order control packet on tunnel 3471 (got 2, expected 3)
Aug  9 19:44:19 localhost xl2tpd[7385]: handle_packet: bad control packet!
Aug  9 19:44:21 localhost xl2tpd[7385]: check_control: Received out of order control packet on tunnel 3471 (got 2, expected 3)
Aug  9 19:44:21 localhost xl2tpd[7385]: handle_packet: bad control packet!
Aug  9 19:44:25 localhost xl2tpd[7385]: check_control: Received out of order control packet on tunnel 3471 (got 2, expected 3)
Aug  9 19:44:25 localhost xl2tpd[7385]: handle_packet: bad control packet!
Aug  9 19:44:33 localhost xl2tpd[7385]: check_control: Received out of order control packet on tunnel 3471 (got 2, expected 3)
Aug  9 19:44:33 localhost xl2tpd[7385]: handle_packet: bad control packet!
Aug  9 19:44:48 localhost xl2tpd[7385]: Maximum retries exceeded for tunnel 23331.  Closing.
Aug  9 19:44:48 localhost xl2tpd[7385]: Terminating pppd: sending TERM signal to pid 7389
Aug  9 19:44:48 localhost xl2tpd[7385]: Connection 3471 closed to [masked_ip_l2tp_server], port 1701 (Timeout)
```

reported here: https://github.com/xelerance/xl2tpd/issues/156

src https://github.com/xelerance/xl2tpd/issues/136
